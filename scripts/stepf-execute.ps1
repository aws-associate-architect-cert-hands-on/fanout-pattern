# Get the state machine ARN
$SM_ARN = `
aws stepfunctions `
list-state-machines `
--query 'stateMachines[*].stateMachineArn' `
--output text

# Start state machine execution
aws stepfunctions `
start-execution `
--state-machine-arn $SM_ARN `
--input '{\"OrderNumber\":\"135421\"}'
