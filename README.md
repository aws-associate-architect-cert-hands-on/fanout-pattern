# AWS Hands-On Fanout Pattern
![banner](images/banner.png)
## Descripción
<p style="text-align:justify">
Repositorio con el codigo y archivos necesarios para automatizar y orquestar un flujo de trabajo serveless dentro de un patrón de distribución ramificada. Se asume un conocimiento básico de las herramientas DevOps con las que se construirá tanto la automatización como orquestación del caso.
</p>

## Diagrama
![diagram](images/diagram.png)

## <b>[Tutorial](https://medium.com/@datamadness81/aws-hands-on-fanout-pattern-928d2e72b163)</b>
## <b>[< Home](https://gitlab.com/aws-associate-architect-cert-hands-on)</b>
